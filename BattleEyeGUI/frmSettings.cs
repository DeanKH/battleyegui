﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

namespace BattleEyeGUI
{
    public partial class frmSettings : Form
    {
        private Form1 parentForm;
        public frmSettings(Form1 form1)
        {
            InitializeComponent();
            parentForm = form1;
            String alwaysOnTop = Properties.Settings.Default.always_on_top;
            if (alwaysOnTop == "1")
            {
                chkAlwaysOnTop.Checked = true;
            }


            String showPlayers = Properties.Settings.Default.show_players;
            if (showPlayers == "1")
            {
                chkShowPlayers.Checked = true;
            }
            txtUsername.Text = Properties.Settings.Default.username;
        }

        private void chkAlwaysOnTop_CheckedChanged(object sender, EventArgs e)
        {
            Form1 frm = new Form1();

            if (this.chkAlwaysOnTop.Checked)
            {
                Properties.Settings.Default.always_on_top = "1";
                frm.TopMost = true;
            }
            else
            {
                Properties.Settings.Default.always_on_top = "0";
                frm.TopMost = false;
            }
        }

        private void btnCloseSettings_Click(object sender, EventArgs e)
        {
            parentForm.checkPlayerListStatus(); 
            parentForm.sayType = "say -1 " + txtUsername.Text + ": ";
            parentForm.username = txtUsername.Text;
            Properties.Settings.Default.username = txtUsername.Text;
            parentForm.checkTopStatus();

            Properties.Settings.Default.Save();

            this.Close();
        }

        private void chkShowPlayers_CheckedChanged(object sender, EventArgs e)
        {
            Form1 frm = new Form1();

            if (this.chkShowPlayers.Checked)
            {
                Properties.Settings.Default.show_players = "1";
            }
            else
            {
                Properties.Settings.Default.show_players = "0";
            }
        }

    }
}
