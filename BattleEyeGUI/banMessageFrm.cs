﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BattleEyeGUI
{
    public partial class banMessageFrm : Form
    {
         private Form1 parentForm;
         public banMessageFrm(Form1 form1)
        {
            InitializeComponent();
            parentForm = form1;
        }

        private void btnKick_Click(object sender, EventArgs e)
        {
            parentForm.banPlayer(parentForm.curPlayerSelected, txtBanMessage.Text, txtBanDuration.Text);
            txtBanMessage.Text = "";
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
