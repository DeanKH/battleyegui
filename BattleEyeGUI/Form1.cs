﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Microsoft.VisualBasic;

using BattleNET;
using System.Collections;
using System.Text.RegularExpressions;
using GameServerInfo;
using System.Diagnostics;

namespace BattleEyeGUI
{
    public partial class Form1 : Form
    {

        private static string ip;
        private static int port;
        private static string password;
        
        BattlEyeClient b;
        private Timer timer1;
        Color bgcolor;
        public String sayType = "";
        public String username;
        Boolean commandMode = false;

        private int lastPlayerListIndex = 0;
        public String curPlayerSelected = "";
        public int playerCount = 0;

        // Used for custom form dragging
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd,
                         int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        DayzClient curUser;

        // This delegate enables asynchronous calls for setting 
        // the text property on a TextBox control. 
        delegate void SetTextCallback(string text);
        delegate void SetTextCallback1(int playerNum, String[] lines);
        delegate void SetTextCallbackClearPlayers();
        delegate void SetTextCallbackShowConnectPanel(String msg);
        delegate void SetTextCallbackAddPlayer(DayzClient user);
        delegate void SetTextCallBackRemovePlayer(int playerListIndex);

        public Form1()
        {
            InitializeComponent();
        }

        private void sendKeepAlivePacket(object sender, EventArgs e)
        {
            Debug.WriteLine("Players command sent");
            b.SendCommand("players");
            while (b.CommandQueue > 0) { /* wait until server received packet */ };
            //b.SendCommand("players");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            String alwaysOnTop = Properties.Settings.Default.always_on_top;
            if (alwaysOnTop == "1")
            {
                this.TopMost = true;
            }
            String showPlayers = Properties.Settings.Default.show_players;
            if (showPlayers == "0")
            {
                this.Visible = false;
            }

            ip = getIP();
            port = getPort();
            password = getPassword();
            txtIP.Text = ip;
            txtPort.Text = port.ToString();
            txtPassword.Text = password;
            curUser = new DayzClient();
        }

        private void connectbtn_Click(object sender, EventArgs e)
        {
            pnlConnectControls.Visible = false;
            lstPlayers.Items.Clear();
            ip = txtIP.Text;
            port = Convert.ToInt16(txtPort.Text);
            password = txtPassword.Text;

            timer1 = new Timer();
            timer1.Tick += new EventHandler(sendKeepAlivePacket);
            timer1.Interval = 10000; // in miliseconds

            rtbText.AppendText(" Connecting...\n");

            BattlEyeLoginCredentials loginCredentials = GetLoginCredentials();
            b = new BattlEyeClient(loginCredentials);
            b.BattlEyeMessageReceived += DumpMessage;
            b.BattlEyeDisconnected += Disconnected;
            b.ReconnectOnPacketLoss = true;
            b.Connect();
            if (b.Connected)
            {
                rtbText.AppendText(" Connected...\n");
                timer1.Start();
            }
            username = getUsername();

            sayType = "say -1 " + username + ": ";

            connectbtn.Text = "Connected";
            connectbtn.Enabled = false;

            //GameServer server = new GameServer(ip, 1717, GameType.GameSpy2);
            //server.DebugMode = true;
            //server.QueryServer();
            //if (!server.IsOnline)
            //{
            //    rtbText.AppendText("Server Offline");
            //}
            //else
            //{
            //    rtbText.AppendText("Server online");
            //}
            //int foo = server.NumPlayers;

            //rtbText.AppendText(" Players: " + foo.ToString());
        }

        private static BattlEyeLoginCredentials GetLoginCredentials()
        {

            var loginCredentials = new BattlEyeLoginCredentials
            {
                Host = ip,
                Port = port,
                Password = password,
            };

            return loginCredentials;
        }

        private void Disconnected(BattlEyeDisconnectEventArgs args)
        {
            doDisconnect(args.Message);
        }

        private void doDisconnect(String msg)
        {
            //connectbtn.Text = "Connect";
            //connectbtn.Enabled = true;
            if (this.rtbText.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(doDisconnect);
                this.Invoke(d, new object[] { msg + "\n" });
            }
            else
            {
                connectbtn.Text = "Connect";
                connectbtn.Enabled = true;
                pnlConnectControls.Visible = true;
                this.rtbText.AppendText(msg + "\n", bgcolor);
            }

        }

        private void DumpMessage(BattlEyeMessageEventArgs args)
        {
            doDumpMessage(args.Message);
        }

        private void doDumpMessage(String msg)
        {
            if (msg.Substring(0, 10) == "Players on")
            {
                getPlayerList(msg);
            }
            else
            {
                // Determines the color that the console should show depending on the type of message received
                setTextColor(msg);

                // Now we check for a connection string so we can manage the player list as users leave/join

                /*
                 *  TO DO:
                 *  1. Create new DayzClient object at global level
                 *  2. Listen for first connection:  Player #24 XForrest (86.142.195.200:2304) connected
                 *  3. Store player ID + name (optional: IP)
                 *  4. Listen for second part of connection:  Verified GUID (fd6f1da93fc9b89fa988c11c7a348023) of player #24 XForrest
                 *  5. Grab the GUID
                 *  6. If both cases above match, we can add the player to the player list using the DayzClient object
                 * 
                 */

                /*
                Match matchString;
                matchString = Regex.Match(msg, @"\bVerified GUID \((?<guid>.+)\) of player #(?<player_id>[0-9]{1,3})\s(?<user>.+)", RegexOptions.IgnoreCase);
                if (matchString.Success)
                {
                    //curUser.IP = matchString.Groups["ip"].Value;
                    curUser.UserName = matchString.Groups["user"].Value;
                    curUser.playerNo = Convert.ToInt32(matchString.Groups["player_id"].Value);
                    curUser.GUID = matchString.Groups["guid"].Value;
                    addPlayer(curUser);
                }
                else
                {
                    // check for a disconnection string so we can remove the user
                    matchString = Regex.Match(msg, @"\bPlayer #(?<player_id>[0-9]{1,3})\s(?<user>.+) disconnected", RegexOptions.IgnoreCase);
                    if (matchString.Success)
                    {
                        int index = lstPlayers.FindString("[" + matchString.Groups["player_id"].Value + "] " + matchString.Groups["user"].Value);
                        // Determine if a valid index is returned. Select the item if it is valid.
                        if (index != -1)
                        {
                            removePlayer(index);
                        }
                        else
                        {
                            index = lstPlayers.FindString("[" + matchString.Groups["player_id"].Value + "] " + matchString.Groups["user"].Value + " (Lobby)");
                            if (index != -1)
                            {
                                removePlayer(index);
                            }
                        }
                    }
                }
                */

                if (this.rtbText.InvokeRequired)
                {
                    SetTextCallback d = new SetTextCallback(doDumpMessage);
                    this.Invoke(d, new object[] { msg });
                }
                else
                {
                    this.rtbText.AppendText(msg, bgcolor);
                }
            }
        }

        private void setTextColor(String msg)
        {
            String text = msg.Substring(0, 5);

            switch (text)
            {
                case "(Side":
                    bgcolor = Color.Cyan;
                    break;
                case "RCon ":
                    bgcolor = Color.Red;
                    break;
                case "(Dire":
                    bgcolor = Color.White;
                    break;
                case "(Vehi":
                    bgcolor = Color.Yellow;
                    break;
                case "(Glob":
                    bgcolor = Color.LightBlue;
                    break;
                case "(Grou":
                    bgcolor = Color.LightGoldenrodYellow;
                    break;
                default:
                    bgcolor = Color.Gray;
                    break;
            }
        }

        private void Form1_Leave(object sender, EventArgs e)
        {
            b.Disconnect();
        }

        private void txtCmd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                string cmd = txtCmd.Text;
                txtCmd.Text = "";
                if (cmd == "exit" || cmd == "logout")
                {
                    //Environment.Exit(0);
                }

                if (b.Connected)
                {
                    b.SendCommand(sayType + cmd);
                    while (b.CommandQueue > 0) { /* wait until server received packet */ };
                }
                else
                {
                    rtbText.AppendText("Not connected!\n");
                }
            }
            else if (e.KeyChar == (char)1)
            {
                switchCommandMode();
            }
        }

        private void Form1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Exit();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        public string getUsername()
        {
            return Properties.Settings.Default.username;
        }

        public static string getIP()
        {
            return Properties.Settings.Default.ip;
        }

        public static int getPort()
        {
            String p = Properties.Settings.Default.port;
            return Convert.ToInt16(p);
        }

        public static string getPassword()
        {
            return Properties.Settings.Default.rconPass;
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            frmSettings frmSettings = new frmSettings(this);
            frmSettings.ShowDialog(this);
        }

        private void btnMaximise_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        public void checkTopStatus()
        {
            String alwaysOnTop = Properties.Settings.Default.always_on_top;
            if (alwaysOnTop == "1")
            {
                this.TopMost = true;
            }
            else
            {
                this.TopMost = false;
            }
        }

        public void checkPlayerListStatus()
        {
            String showPlayers = Properties.Settings.Default.show_players;
            if (showPlayers == "1")
            {
                lstPlayers.Visible = true;
            }
            else
            {
                lstPlayers.Visible = false;
            }
        }

        private void getPlayerList(String playerList)
        {
            try
            {

                String[] lines = playerList.Split(new string[] { "\n" }, StringSplitOptions.None);
                String[] players = new String[lines.Length - 3];

                clearPlayerList();
                playerCount = 1;

                for (int i = 3; i <= lines.Length - 2; i++)
                {
                    addPlayerToList(i, lines);
                    playerCount++;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("Caught exception: " + e.ToString());
            }
            finally
            {
                Debug.WriteLine("finally finished.");
            }
        }

        private void addPlayer(DayzClient user)
        {
            if (this.lstPlayers.InvokeRequired)
            {
                SetTextCallbackAddPlayer d = new SetTextCallbackAddPlayer(addPlayer);
                this.Invoke(d, new object[] { user });
            }
            else
            {
                int index = lstPlayers.FindString("[" + user.playerNo + "] " + user.UserName);
                // Determine if a valid index is returned. Select the item if it is valid.
                if (index == -1)
                {
                    this.lstPlayers.Items.Add("[" + user.playerNo + "] " + user.UserName);
                    playerCount++;
                    lblPlayerCount.Text = "Players: " + playerCount.ToString();
                }
            }

        }

        private void removePlayer(int playerListIndex)
        {
            if (this.lstPlayers.InvokeRequired)
            {

                SetTextCallBackRemovePlayer d = new SetTextCallBackRemovePlayer(removePlayer);
                this.Invoke(d, new object[] { playerListIndex });
            }
            else
            {
                lstPlayers.Items.RemoveAt(playerListIndex);
            }

        }

        private void addPlayerToList(int playerNum, String[] lines)
        {
            // player list indexes
            // 0 = id
            // 1 = ip/port
            // 2 = ping
            // 3 = guid
            // 4 = username

            String line = lines[playerNum];

            Match matchString;
            matchString = Regex.Match(line, @"(?<player_id>[0-9]{1,3})\s+(?<ip>.+):(?<port>[0-9]{1,5})\s+(?<ping>[0-9]+)\s+(?<guid>.+)\(OK\)(?<name>.+)", RegexOptions.IgnoreCase);

            if (this.lstPlayers.InvokeRequired)
            {
                SetTextCallback1 d = new SetTextCallback1(addPlayerToList);
                this.Invoke(d, new object[] { playerNum, lines });
            }
            else
            {
                this.lstPlayers.Items.Add("[" + matchString.Groups["player_id"].Value + "] " + matchString.Groups["name"].Value);
                if ((playerNum - 3) == lastPlayerListIndex)
                {
                    this.lstPlayers.SetSelected(lastPlayerListIndex, true);
                }
                lblPlayerCount.Text = "Players: " + playerCount.ToString();
            }

        }

        private void clearPlayerList()
        {
            if (this.lstPlayers.InvokeRequired)
            {
                SetTextCallbackClearPlayers d = new SetTextCallbackClearPlayers(clearPlayerList);
                this.Invoke(d, new object[] { });
            }
            else
            {
                lastPlayerListIndex = this.lstPlayers.SelectedIndex;
                this.lstPlayers.Items.Clear();
            }
        }

        private void lblCommandStyle_Click(object sender, EventArgs e)
        {
            switchCommandMode();
        }

        private void switchCommandMode()
        {
            if (!commandMode)
            {
                lblCommandStyle.ForeColor = Color.LightGray;
                lblCommandStyle.Text = "COMMAND";
                sayType = "";
                commandMode = true;
            }
            else
            {
                lblCommandStyle.ForeColor = Color.Cyan;
                lblCommandStyle.Text = "GLOBAL";
                sayType = "say -1 " + username + ": ";
                commandMode = false;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                this.TopMost = true;
            }
            else
            {
                this.TopMost = false;
            }
        }

        private void kickToolStripMenuItem_Click(object sender, EventArgs e)
        {
            String kickString = "";
            String user = lstPlayers.Items[lstPlayers.SelectedIndex].ToString();

            Match matchString;
            matchString = Regex.Match(user, @"(?<player_id>[0-9]{1,3})", RegexOptions.IgnoreCase);

            if (matchString.Success)
            {
                kickString = "kick " + matchString.Groups["player_id"];
            }

            if (MessageBox.Show("Kick user #"+matchString.Groups["player_id"]+"?", "Confirm kick", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                // a 'DialogResult.Yes' value was returned from the MessageBox
                // proceed with your deletion
                b.SendCommand(kickString);
                while (b.CommandQueue > 0) { /* wait until server received packet */ };
            }
        }

        private void kickwithReasonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            getSelectedPlayerID();

            kickMessageFrm kickMsg = new kickMessageFrm(this);

            kickMsg.ShowDialog(this);
        }

        public void kickPlayer(String playerID, String kickMessage)
        {
            String kickString = "";
            kickString = "kick " + playerID + " " + kickMessage;
            b.SendCommand(kickString);
            while (b.CommandQueue > 0) { /* wait until server received packet */ };
        }

        private void banToolStripMenuItem_Click(object sender, EventArgs e)
        {
            String banString = "";
            String user = lstPlayers.Items[lstPlayers.SelectedIndex].ToString();

            Match matchString;
            matchString = Regex.Match(user, @"(?<player_id>[0-9]{1,3})", RegexOptions.IgnoreCase);

            if (matchString.Success)
            {
                banString = "ban " + matchString.Groups["player_id"];
            }

            if (MessageBox.Show("Ban user #" + matchString.Groups["player_id"] + "?", "Confirm perma-ban", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                // a 'DialogResult.Yes' value was returned from the MessageBox
                // proceed with your deletion
                b.SendCommand(banString);
                while (b.CommandQueue > 0) { /* wait until server received packet */ };
            }
        }

        private void banwithReasonToolStripMenuItem_Click(object sender, EventArgs e)
        {
            getSelectedPlayerID();

            banMessageFrm banMsg = new banMessageFrm(this);
            banMsg.ShowDialog(this);
        }

        public void banPlayer(String playerID, String kickMessage, String banDuration)
        {
            String kickString = "";
            kickString = "ban " + playerID + " " + banDuration + " " + kickMessage;
            b.SendCommand(kickString);
            while (b.CommandQueue > 0) { /* wait until server received packet */ };
        }

        private void messageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            getSelectedPlayerID();

            sendMessageFrm sendMsg = new sendMessageFrm(this);
            sendMsg.ShowDialog(this);
        
        }

        private void getSelectedPlayerID()
        {
            String user = lstPlayers.Items[lstPlayers.SelectedIndex].ToString();
            lstPlayers.Items[lstPlayers.SelectedIndex].ToString();

            Match matchString;
            matchString = Regex.Match(user, @"(?<player_id>[0-9]{1,3})", RegexOptions.IgnoreCase);

            if (matchString.Success)
            {
                curPlayerSelected = matchString.Groups["player_id"].ToString();
            }
        }

        public void msgPlayer(String playerID, String Message)
        {
            String msgString = "";
            msgString = "say "+ playerID + " " + username + ": "  + Message;
            b.SendCommand(msgString);
            while (b.CommandQueue > 0) { /* wait until server received packet */ };
        }

        private void lstPlayers_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right) {
                var item = lstPlayers.IndexFromPoint(e.Location);
                if (item >= 0) {
                    lstPlayers.SelectedIndex = item;
                    ctxPlayerOptions.Show(lstPlayers, e.Location);
                }
            }
        }

        private void lblRefresh_Click(object sender, EventArgs e)
        {
            b.SendCommand("players");
            while (b.CommandQueue > 0) { /* wait until server received packet */ };
        }


    }
    
    public class DayzClient
    {
        public string GUID { get; set; }
        public string IP { get; set; }
        public string UserName { get; set; }
        public string message { get; set; }
        public string email { get; set; }
        public int playerNo { get; set; }

        public LogTypes logType { get; set; }

        public enum LogTypes
        {
            Success = 1,
            Kick = 2
        }
    }

    public static class RichTextBoxExtensions
    {
        public static void AppendText(this RichTextBox box, string text, Color color)
        {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;

            box.SelectionColor = color;
            box.AppendText(" " + text + "\n");
            box.SelectionColor = box.ForeColor;
            box.SelectionStart = box.Text.Length;
            box.ScrollToCaret();
        }
    }

}
