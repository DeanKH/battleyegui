﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BattleEyeGUI
{
    public partial class kickMessageFrm : Form
    {
         private Form1 parentForm;
         public kickMessageFrm(Form1 form1)
        {
            InitializeComponent();
            parentForm = form1;
        }

        private void btnKick_Click(object sender, EventArgs e)
        {
            parentForm.kickPlayer(parentForm.curPlayerSelected, txtKickMessage.Text);
            txtKickMessage.Text = "";
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
